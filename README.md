import requests
import json

with open ('ips.txt') as input_a:
    list_of_ip = (input_a.read()).split('\n')
    
list_of_keys = {}
full_list_of_countys = []

for ip in list_of_ip:
    response = dict(requests.get ("http://ip-api.com/json/" + ip).json())
    list_of_keys[response.get("country", "country was not found")] = ip
    full_list_of_countys.append(response.get("country", "country was not found"))

index_ip = 0
file = open("output.txt", "w")
file.close()

sorted_country = list(list_of_keys.keys())
sorted_country.sort()

for element_unique_country in sorted_country:
    with open ("output.txt", "a") as in_file:
        in_file.write (str(element_unique_country) + ":   ")
    for element_country in full_list_of_countys:   
        if element_unique_country == element_country:
            with open ("output.txt", "a") as in_file:
                in_file.write (str(list_of_ip[index_ip]) + "; ")            
        index_ip = index_ip + 1  
        if index_ip == len(full_list_of_countys):
            with open ("output.txt", "a") as in_file:
                in_file.write ("\n") 
            index_ip = 0